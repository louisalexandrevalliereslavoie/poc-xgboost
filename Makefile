# Makefile
pythonPath := python -c "import sys; print(sys.executable)"



# This creates a conda environment named as pdfextract
env:
	conda env create --file environment.yml  --prefix  $(HOME)/.conda/envs/example-xgboost

update:
	conda env update --name pdfextract --file environment.yml
	python -m pip install .

test:
	pytest -v --cov-report term-missing --cov=src tests/
