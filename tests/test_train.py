import pytest
import logging
import os
import sys
from src.train import setupLogging, pathExist, load_dataset, split_dataset, train_model, save_model, get_predictions, get_accuracy, main

def testdirPathExist():
    path = "src/"
    assert pathExist(path) == True

def testfilePathExist():
    path = "src/__init__.py"
    assert pathExist(path) == True

def testNotFilePathExist():
    path = "src/canard.py"
    assert pathExist(path) == False

def testNotDirPathExist():
    path = "canard/"
    assert pathExist(path) == False

def testSetupLogging():
    setupLogging()
    assert logging.root.level == logging.ERROR

def testSetupLoggingVerbose():
    setupLogging(True)
    assert logging.root.level == logging.DEBUG
