ARG BASE_IMG_TAG
ARG BASE_IMG_NAME
FROM $BASE_IMG_NAME:$BASE_IMG_TAG

ADD ./ /app

WORKDIR /app

RUN make env && source activate xgboost-example
RUN make test
