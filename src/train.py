#Taken from https://machinelearningmastery.com/develop-first-xgboost-model-python-scikit-learn/
from numpy import loadtxt
from xgboost import XGBClassifier
import pickle
import os
import logging
import fire
import sys
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

def setupLogging(verbose: bool=False):
    root = logging.getLogger()
    handler = logging.StreamHandler(sys.stdout)

    if(verbose):
        handler.setLevel(logging.DEBUG)
        root.setLevel(logging.DEBUG)
    else:
        handler.setLevel(logging.ERROR)
        root.setLevel(logging.ERROR)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)

def pathExist(givenPath: str) -> bool:
    logging.debug(f'Verifying path: {givenPath}')
    if os.path.exists(givenPath):
        return True
    else:
        return False

def load_dataset(dataset_path: str):
    logging.debug(f'Loading dataset from path: {dataset_path}')
    return loadtxt(dataset_path, delimiter=",") if pathExist(dataset_path) else None

def split_dataset(dataset, test_size: float, seed: int):
    logging.debug(f'Splitting dataset with test size of: {test_size} and seed of: {seed}')
    X = dataset[:,0:8]
    Y = dataset[:,8]
    return train_test_split(X, Y, test_size=test_size, random_state=seed)

def train_model(x_train, y_train):
    logging.debug(f'Starting model training')
    model = XGBClassifier()
    model.fit(x_train, y_train)
    logging.debug(f'Model trained')
    return model

def save_model(model, output_path: str):
    logging.debug(f'Saving model to: {output_path}')
    if pathExist(output_path):
        pickle.dump(model, open(output_path, "wb"))
    else:
        logging.error(f'{output_path} not valid')

def get_predictions(model, x_test):
    logging.debug(f'Getting prediction from model')
    y_pred = model.predict(x_test)
    return [round(value) for value in y_pred]

def get_accuracy(predictions, y_test):
    logging.debug(f'Calculating accuracy score')
    return accuracy_score(y_test, predictions)

def main(dataset_path:str = '../pima-indians-diabetes.csv', output_path:str ='../pima.pickle.dat', test_size:float = 0.33, seed:int = 7, verbose:bool = True):
    setupLogging(verbose)
    dataset = load_dataset(dataset_path)
    X_train, X_test, y_train, y_test = split_dataset(dataset, test_size, seed)
    model = train_model(X_train, y_train)
    save_model(model, output_path)
    accuracy = get_accuracy(get_predictions(model, X_test), y_test)
    print("Accuracy: %.2f%%" % (accuracy * 100.0))


if __name__ == "__main__":
    fire.Fire(main)
